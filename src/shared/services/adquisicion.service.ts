import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from '../../environments/environment ';
import { Adquisicion } from '../models/adquisicion';
import { Observable } from 'rxjs';
import { Historial } from '../models/historial';

@Injectable({
  providedIn: 'root',
})
export class AdquisicionService {
  private readonly http: HttpClient = inject(HttpClient);
  private readonly apiUrl: string = `${environment.urlApi}Adquisicion`;

  getAll(): Observable<Adquisicion[]> {
    return this.http.get<Adquisicion[]>(this.apiUrl);
  }

  saveAdquisicion(data: Adquisicion): Observable<any> {
    return this.http.post(this.apiUrl, data);
  }

  getAquisicion(id: number): Observable<Adquisicion> {
    return this.http.get<Adquisicion>(`${this.apiUrl}/${id}`);
  }

  updatedAdqui(id: number, dataForm: Adquisicion): Observable<Adquisicion> {
    return this.http.put<Adquisicion>(`${this.apiUrl}/${id}`, dataForm);
  }

  getHistorial(id: number): Observable<Historial[]> {
    return this.http.get<Historial[]>(`${this.apiUrl}/${id}/historial`);
  }

  deleteAdq(id: number) {
    return this.http.delete<Adquisicion>(`${this.apiUrl}/${id}`);
  }
}
