export interface Historial {
  id: number;
  createAt: string;
  action: string;
  oldValues: string;
  newValues: string;
}
