export interface Adquisicion {
  id: number;
  presupuesto: number;
  unidad: string;
  tipo: string;
  catidad: number;
  valorUnit: number;
  valorTotal: number;
  fechaAdquisicion: string;
  proveedor: string;
  documentacion: string;
}
