import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./adquisicion/adquisicion.module').then(
            (m) => m.AdquisicionModule
          ),
      },
    ],
  },
];
