import {
  ApplicationConfig,
  LOCALE_ID,
  importProvidersFrom,
} from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';

import es from '@angular/common/locales/es-CO';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

registerLocaleData(es);

export const appConfig: ApplicationConfig = {
  providers: [
    { provide: LOCALE_ID, useValue: 'es-CO' },
    provideRouter(routes),
    provideAnimationsAsync(),
    importProvidersFrom(HttpClientModule, SweetAlert2Module.forRoot()),
  ],
};
