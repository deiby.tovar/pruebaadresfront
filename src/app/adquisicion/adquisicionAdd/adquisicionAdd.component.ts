import { Component, OnInit } from '@angular/core';
import { TypeForm } from '../formAdq/formAdq.general';
import { Adquisicion } from '../../../shared/models/adquisicion';
import { AdquisicionService } from '../../../shared/services/adquisicion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adquisicionAdd',
  templateUrl: './adquisicionAdd.component.html',
  styleUrls: ['./adquisicionAdd.component.scss'],
})
export class AdquisicionAddComponent implements OnInit {
  type = TypeForm.ADD;

  dataForm!: Adquisicion;
  constructor(
    private _aquisicionService: AdquisicionService,
    private router: Router
  ) {}

  ngOnInit() {}

  saveForm(data: Adquisicion) {
    this.dataForm = data;
    this._aquisicionService.saveAdquisicion(this.dataForm).subscribe({
      next: (resp) => {
        this.router.navigate(['/']);
      },
    });
  }
}
