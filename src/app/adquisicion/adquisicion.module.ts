import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdquisicionComponent } from './adquisicion.component';
import { AdquisicionRoutingModule } from './adquisicion-routing.module';
import { MaterialModuleModule } from '../../shared/sharedModule/MaterialModule/MaterialModule.module';
import { AdquisicionAddComponent } from './adquisicionAdd/adquisicionAdd.component';
import { FormAdqComponent } from './formAdq/formAdq.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdquisicionEditComponent } from './adquisicionEdit/adquisicionEdit.component';
import { AdquisicionViewComponent } from './adquisicionView/adquisicionView.component';
import { HistorialDetailComponent } from './adquisicionView/historialDetail/historialDetail.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  imports: [
    CommonModule,
    MaterialModuleModule,
    AdquisicionRoutingModule,
    ReactiveFormsModule,
    SweetAlert2Module,
  ],
  declarations: [
    AdquisicionComponent,
    AdquisicionAddComponent,
    FormAdqComponent,
    AdquisicionEditComponent,
    AdquisicionViewComponent,
    HistorialDetailComponent,
  ],
})
export class AdquisicionModule {}
