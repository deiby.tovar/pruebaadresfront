import { Component, OnInit } from '@angular/core';
import { TypeForm } from '../formAdq/formAdq.general';
import { Adquisicion } from '../../../shared/models/adquisicion';
import { AdquisicionService } from '../../../shared/services/adquisicion.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-adquisicionEdit',
  templateUrl: './adquisicionEdit.component.html',
  styleUrls: ['./adquisicionEdit.component.scss'],
})
export class AdquisicionEditComponent implements OnInit {
  type = TypeForm.EDIT;

  id = this.activateRouter.snapshot.paramMap.get('id');

  constructor(
    private _aquisicionService: AdquisicionService,
    private router: Router,
    private activateRouter: ActivatedRoute
  ) {}

  ngOnInit() {}

  saveForm(dataForm: Adquisicion) {
    this._aquisicionService.updatedAdqui(Number(this.id), dataForm).subscribe({
      next: (resp) => {
        this.router.navigate(['/']);
      },
    });
  }
}
