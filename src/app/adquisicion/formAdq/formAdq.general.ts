import { FormControl } from '@angular/forms';

export enum TypeForm {
  ADD = 'add',
  EDIT = 'edit',
  VIEW = 'view',
}

export interface FormAdq {
  presupuesto: FormControl<number | null>;
  unidad: FormControl<string | null>;
  tipo: FormControl<string | null>;
  catidad: FormControl<number | null>;
  valorUnit: FormControl<number | null>;
  valorTotal: FormControl<number | null>;
  fechaAdquisicion: FormControl<string | null>;
  proveedor: FormControl<string | null>;
  documentacion: FormControl<string | null>;
}
