import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormAdq, TypeForm } from './formAdq.general';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Adquisicion } from '../../../shared/models/adquisicion';
import { formatDate } from '@angular/common';
import { AdquisicionService } from '../../../shared/services/adquisicion.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-formAdq',
  templateUrl: './formAdq.component.html',
  styleUrls: ['./formAdq.component.scss'],
})
export class FormAdqComponent implements OnInit {
  @Input() type!: TypeForm;

  @Output() saveDataForm: EventEmitter<Adquisicion> = new EventEmitter();

  id = this.activateRouter.snapshot.paramMap.get('id');

  formGroup: FormGroup;
  constructor(
    private fb: FormBuilder,
    private _aquisicionService: AdquisicionService,
    private activateRouter: ActivatedRoute
  ) {
    this.formGroup = this.fb.group<FormAdq>({
      presupuesto: new FormControl(null, Validators.required),
      catidad: new FormControl(null, Validators.required),
      documentacion: new FormControl('', Validators.required),
      fechaAdquisicion: new FormControl('', Validators.required),
      proveedor: new FormControl('', Validators.required),
      tipo: new FormControl('', Validators.required),
      unidad: new FormControl('', Validators.required),
      valorUnit: new FormControl(null, Validators.required),
      valorTotal: new FormControl(0),
    });
  }

  ngOnInit() {
    if (this.type != TypeForm.ADD) {
      this._aquisicionService.getAquisicion(Number(this.id)).subscribe({
        next: (resp) => {
          resp.fechaAdquisicion = formatDate(
            resp.fechaAdquisicion,
            'yyyy-MM-dd',
            'es-CO'
          );
          this.formGroup.patchValue(resp);
        },
      });
      if (this.type == TypeForm.VIEW) {
        Object.keys(this.formGroup.controls).forEach((key) => {
          this.formGroup.controls[key].disable();
        });
      }
    }
  }

  calculateTotal() {
    if (
      this.formGroup.get('valorUnit')?.value &&
      this.formGroup.get('catidad')?.value
    ) {
      const total =
        this.formGroup.get('valorUnit')?.value *
        this.formGroup.get('catidad')?.value;

      this.formGroup.get('valorTotal')?.setValue(total);
    } else {
      this.formGroup.get('valorTotal')?.setValue(0);
    }
  }

  saveForm() {
    if (this.formGroup.valid) {
      const data = this.formGroup.value as Adquisicion;
      data.fechaAdquisicion = formatDate(
        data.fechaAdquisicion,
        'dd/MM/YYYY',
        'es-CO'
      );
      this.saveDataForm.emit(data);
    } else {
      Object.keys(this.formGroup.controls).forEach((key) => {
        this.formGroup.controls[key].markAsDirty();
        this.formGroup.controls[key].markAsTouched();
      });
    }
  }
}
