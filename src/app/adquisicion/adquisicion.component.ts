import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { AdquisicionService } from '../../shared/services/adquisicion.service';
import { Adquisicion } from '../../shared/models/adquisicion';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-adquisicion',
  templateUrl: './adquisicion.component.html',
  styleUrls: ['./adquisicion.component.scss'],
})
export class AdquisicionComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [
    'id',
    'presupuesto',
    'unidad',
    'tipo',
    'total',
    'fecha',
    'acciones',
  ];
  dataSource = new MatTableDataSource<Adquisicion>([]);

  deleteAdRe: Adquisicion | null = null;

  @ViewChild('deleteSwal') deleteSwal!: SwalComponent;

  constructor(private _adquisicionService: AdquisicionService) {}

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.cargarList();
  }

  cargarList() {
    this._adquisicionService.getAll().subscribe((resp) => {
      this.dataSource.data = resp;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openConfirm(element: Adquisicion) {
    this.deleteAdRe = element;
    this.deleteSwal.fire();
  }

  deleteAd() {
    this._adquisicionService.deleteAdq(this.deleteAdRe!.id).subscribe({
      next: () => {
        this.cargarList();
      },
    });
  }
}
