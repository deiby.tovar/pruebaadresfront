import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdquisicionComponent } from './adquisicion.component';
import { AdquisicionAddComponent } from './adquisicionAdd/adquisicionAdd.component';
import { AdquisicionEditComponent } from './adquisicionEdit/adquisicionEdit.component';
import { AdquisicionViewComponent } from './adquisicionView/adquisicionView.component';

const routes: Routes = [
  { path: '', component: AdquisicionComponent },
  {
    path: 'add',
    component: AdquisicionAddComponent,
  },

  {
    path: 'edit/:id',
    component: AdquisicionEditComponent,
  },
  {
    path: 'view/:id',
    component: AdquisicionViewComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdquisicionRoutingModule {}
