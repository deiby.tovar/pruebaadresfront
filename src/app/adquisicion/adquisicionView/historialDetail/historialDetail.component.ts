import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Historial } from '../../../../shared/models/historial';

@Component({
  selector: 'app-historialDetail',
  templateUrl: './historialDetail.component.html',
  styleUrls: ['./historialDetail.component.scss'],
})
export class HistorialDetailComponent implements OnInit {
  oldValues: any;
  oldValuesKeys: string[] = [];

  newValues: any;
  newValueKeys: string[] = [];
  constructor(
    public dialogRef: MatDialogRef<HistorialDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Historial
  ) {}

  ngOnInit() {
    this.oldValues = this.data.oldValues ? JSON.parse(this.data.oldValues) : {};
    this.oldValuesKeys = Object.keys(this.oldValues);
    this.newValues = this.data.newValues ? JSON.parse(this.data.newValues) : {};
    this.newValueKeys = Object.keys(this.newValues);
  }

  close() {
    this.dialogRef.close();
  }
}
