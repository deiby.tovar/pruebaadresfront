import { Component, OnInit } from '@angular/core';
import { TypeForm } from '../formAdq/formAdq.general';
import { AdquisicionService } from '../../../shared/services/adquisicion.service';
import { ActivatedRoute } from '@angular/router';
import { Historial } from '../../../shared/models/historial';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { HistorialDetailComponent } from './historialDetail/historialDetail.component';

@Component({
  selector: 'app-adquisicionView',
  templateUrl: './adquisicionView.component.html',
  styleUrls: ['./adquisicionView.component.scss'],
})
export class AdquisicionViewComponent implements OnInit {
  type: TypeForm = TypeForm.VIEW;

  id = this.activateRouter.snapshot.paramMap.get('id');

  displayedColumns: string[] = ['id', 'accion', 'realizado', 'acciones'];
  dataSource = new MatTableDataSource<Historial>([]);

  constructor(
    private _aquisicionService: AdquisicionService,
    private activateRouter: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this._aquisicionService.getHistorial(Number(this.id)).subscribe({
      next: (resp) => {
        this.dataSource.data = resp;
      },
    });
  }

  openHistorial(data: Historial): void {
    const dialogRef = this.dialog.open(HistorialDetailComponent, {
      data: data,
      width: '800px',
    });
  }
}
